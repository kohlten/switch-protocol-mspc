use switch_protocol::internal::Protocol;
use crate::mspc_socket::MspcSocket;
use std::collections::VecDeque;
use switch_protocol::{Client, SwitchError};

pub struct MspcClient {
    conn: Protocol<MspcSocket, ()>,
    events: VecDeque<Vec<u8>>,
}

impl MspcClient {
    pub fn new(conn: MspcSocket) -> Self {
        Self {
            conn: Protocol::new(conn),
            events: Default::default()
        }
    }
}

impl Client for MspcClient {
    type Address = ();

    fn update(&mut self) -> Result<(), SwitchError> {
        self.conn.update()?;
        while let Some(packet) = self.conn.poll() {
            self.events.push_front(packet);
        }
        Ok(())
    }

    fn send(&mut self, packet: Vec<u8>) -> Result<(), SwitchError> {
        self.conn.send(packet)
    }

    fn poll(&mut self) -> Option<Vec<u8>> {
        self.events.pop_back()
    }

    fn peer_addr(&self) -> Result<Self::Address, SwitchError> {
        Ok(())
    }

    fn local_addr(&self) -> Result<Self::Address, SwitchError> {
        Ok(())
    }
}