use std::error::Error;
use std::fmt::{Debug, Formatter, Display};

mod mspc_socket;
mod mspc_client;
mod mspc_server;

#[derive(Copy, Clone, Debug)]
pub enum MspcError {
    Disconnected
}

impl Display for MspcError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            MspcError::Disconnected => {
                f.write_str("Socket was disconnected")
            }
        }
    }
}

impl Error for MspcError {}

use mspc_socket::MspcSocket;
use mspc_client::MspcClient;
use mspc_server::MspcServer;

#[cfg(test)]
mod tests {
    use crate::mspc_server::MspcServer;
    use switch_protocol::{Client, Server, ServerEvent};
    use std::time::{Instant, Duration};

    #[test]
    fn overall() {
        const ITEMS_TO_SEND: usize = 5;

        let mut server = MspcServer::new();
        let mut client = server.accept_client();
        let mut items = 0;

        assert!(client.send("Hello world!".as_bytes().to_vec()).is_ok());

        let st = Instant::now();

        while items < 5 {
            assert!(server.update().is_ok());
            assert!(client.update().is_ok());
            while let Some(event) = server.poll() {
                match event {
                    ServerEvent::ReceivedPacket(packet) => {
                        items += 1;
                        assert!(server.send(packet.get_id(), packet.get_packet().clone()).is_ok());
                    }
                    ServerEvent::ClientConnectionStatus(_) => {}
                }
            }
            while let Some(packet) = client.poll() {
                assert!(client.send(packet).is_ok());
            }
            if Instant::now().duration_since(st) > Duration::from_secs(5) {
                panic!("Took too long!");
            }
        }
    }
}
