use std::io::Write;
use std::sync::mpsc::{Receiver, Sender, TryRecvError};

use switch_protocol::internal::Socket;
use switch_protocol::SwitchError;
use crate::MspcError;

pub struct MspcSocket {
    send: Sender<Vec<u8>>,
    receive: Receiver<Vec<u8>>,
    buffer: Vec<u8>,
}

impl MspcSocket {
    pub fn new(send: Sender<Vec<u8>>, receive: Receiver<Vec<u8>>) -> Self {
        Self {
            send,
            receive,
            buffer: vec![],
        }
    }
}

impl Socket for MspcSocket {
    type Address = ();

    fn read(&mut self, mut buf: &mut [u8]) -> Result<usize, SwitchError> {
        match self.receive.try_recv() {
            Ok(mut data) => {
                self.buffer.append(&mut data);
            }
            Err(err) => {
                match err {
                    TryRecvError::Disconnected => {
                        return Err(SwitchError::Other(Box::new(MspcError::Disconnected)));
                    }
                    TryRecvError::Empty => {}
                }
            }
        };
        let len = if self.buffer.len() > buf.len() {
            buf.len()
        } else {
            self.buffer.len()
        };
        let data = self.buffer[..len].as_ref();
        buf.write(data).unwrap(); // Not expected to fail since it's a memory buffer.
        self.buffer.drain(..len);
        Ok(len)
    }

    fn write(&mut self, buf: &[u8]) -> Result<usize, SwitchError> {
        match self.send.send(buf.to_vec()) {
            Ok(_) => {}
            Err(_) => {
                return Err(SwitchError::Other(Box::new(MspcError::Disconnected)));
            }
        }
        Ok(buf.len())
    }

    fn set_nonblocking(&self, _nonblocking: bool) -> Result<(), SwitchError> {
        Ok(())
    }

    fn local_addr(&self) -> Result<Self::Address, SwitchError> {
        Ok(())
    }

    fn peer_addr(&self) -> Result<Self::Address, SwitchError> {
        Ok(())
    }
}