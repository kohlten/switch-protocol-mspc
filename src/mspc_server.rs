use std::collections::{VecDeque, HashMap};
use switch_protocol::{ServerEvent, Server, SwitchError, Client, ServerPacket, ClientConnectionStatus, ConnectionStatus};
use crate::mspc_client::MspcClient;
use std::sync::mpsc::channel;
use crate::mspc_socket::MspcSocket;

pub struct MspcServer {
    events: VecDeque<ServerEvent>,
    clients: HashMap<String, MspcClient>
}

impl MspcServer {
    pub fn new() -> Self {
        Self {
            events: Default::default(),
            clients: Default::default()
        }
    }

    /// Accepts a new client into the server.
    /// Returns the client to communicate with the server.
    /// The id of the server client can be found using the event poll.
    pub fn accept_client(&mut self) -> MspcClient {
        let (server_sender, client_receiver) = channel();
        let (client_sender, server_receiver) = channel();
        let client = MspcClient::new(MspcSocket::new(client_sender, client_receiver));
        let server_client =  MspcClient::new(MspcSocket::new(server_sender, server_receiver));
        let id = uuid::Uuid::new_v4().to_string();
        self.clients.insert(id.clone(), server_client);
        client
    }
}

impl Server for MspcServer {
    type Address = ();

    fn update(&mut self) -> Result<(), SwitchError> {
        let mut to_be_removed = Vec::new();
        for (id, client) in self.clients.iter_mut() {
            match client.update() {
                Ok(_) => {}
                Err(_) => {
                    to_be_removed.push(id.to_string());
                    continue;
                }
            }
            while let Some(packet) = client.poll() {
                self.events.push_back(ServerEvent::ReceivedPacket(ServerPacket::new(id, packet)));
            }
        }
        for id in to_be_removed.iter() {
            self.clients.remove(id).unwrap();
            self.events.push_back(ServerEvent::ClientConnectionStatus(ClientConnectionStatus::new(id, ConnectionStatus::Disconnected)));
        }
        Ok(())
    }

    fn poll(&mut self) -> Option<ServerEvent> {
        self.events.pop_front()
    }

    fn close_client(&mut self, id: &str) {
        self.clients.remove(id);
        self.events.push_back(ServerEvent::ClientConnectionStatus(ClientConnectionStatus::new(id, ConnectionStatus::Disconnected)));
    }

    fn send(&mut self, id: &str, packet: Vec<u8>) -> Result<(), SwitchError> {
        match self.clients.get_mut(id) {
            None => { Err(SwitchError::NoClientWithId(id.to_string())) }
            Some(client) => { client.send(packet) }
        }
    }

    fn client_peer_addr(&self, _id: &str) -> Result<Self::Address, SwitchError> {
        Ok(())
    }

    fn client_local_addr(&self, _id: &str) -> Result<Self::Address, SwitchError> {
        Ok(())
    }
}